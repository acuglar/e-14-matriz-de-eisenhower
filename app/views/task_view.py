from flask import Blueprint, request
from http import HTTPStatus
from app.services.task_service import create_task, importance_urgency_error


bp = Blueprint('task_route', __name__)


@bp.route('/tasks', methods=['POST'])
def create():
    data = request.get_json()
    
    if data['importance'] > 2 or data['urgency'] > 2:

        return importance_urgency_error(data), HTTPStatus.BAD_REQUEST

    return create_task(data), HTTPStatus.CREATED
    

    