from flask import Blueprint, request
from http import HTTPStatus
from app.services.category_service import create_category


bp = Blueprint('category_route', __name__)


@bp.route('/category', methods=['POST'])
def create():
    data = request.get_json()

    return create_category(data), HTTPStatus.CREATED
