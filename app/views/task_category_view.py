from flask import Blueprint, request
from http import HTTPStatus
from app.services.task_category_service import create_task_category


bp = Blueprint('task_category_route', __name__)


@bp.route('/task_category', methods=['POST'])
def create():
    data = request.get_json()

    return create_task_category(data), HTTPStatus.CREATED