from flask import Blueprint, jsonify
from http import HTTPStatus
from app.services.get_service import get_all


bp = Blueprint('get_route', __name__)


from app.models.eisenhower_model import EisenhowerModel
@bp.route('/')
def home():
    
    return jsonify(get_all()), HTTPStatus.OK