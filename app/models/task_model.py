from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.orm import backref
from app import db

class TaskModel(db.Model):
    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True)

    name = Column(String(100), nullable=False, unique=True)
    description = Column(Text)
    duration = Column(Integer)
    importance = Column(Integer)
    urgency = Column(Integer)

    eisenhower_id = Column(Integer, db.ForeignKey('eisenhowers.id'), nullable=False)

    eisenhower = db.relationship('EisenhowerModel', backref='tasks')
    category = db.relationship("CategoryModel", backref='tasks', secondary='tasks_categories')
    
    """ 
    relashionship 
    1:n -> backref
    n:n -> backref, secondary
    """