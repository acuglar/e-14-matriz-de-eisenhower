from app import db
from sqlalchemy import Column, Integer


class TaskCategoryModel(db.Model):
    __tablename__ = 'tasks_categories'

    id = Column(Integer, primary_key=True)

    task_id = Column(Integer, db.ForeignKey('tasks.id'), nullable=False)
    category_id = Column(Integer, db.ForeignKey('categories.id'), nullable=False)

    