from app import db
from http import HTTPStatus
from app.models.task_model import TaskModel
from app.models.category_model import CategoryModel
from app.models.task_category_model import TaskCategoryModel


def create_task_category(data):

    category = CategoryModel.query.filter_by(name=data['category_name']).first()
    task = TaskModel.query.filter_by(name=data["task_name"]).first()

    task_category = TaskCategoryModel(task_id=task.id, category_id=category.id)

    db.session.add(task_category)
    db.session.commit()


    return {
        "task": task.name,
        "category": category.name,
        "eisenhower_classification": task.eisenhower.type
    }
