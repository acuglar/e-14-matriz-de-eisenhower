from app import db
from app.models.task_model import TaskModel
from app.models.eisenhower_model import EisenhowerModel
from app.services.helpers import eisenhower_classification
from http import HTTPStatus

def create_task(data: dict) -> dict:
    tasks = TaskModel(**data)

    eisenhower_type = eisenhower_classification(tasks.urgency, tasks.importance)

    eisenhower = EisenhowerModel.query.filter_by(type=eisenhower_type).first()

    tasks.eisenhower_id = eisenhower.id

    db.session.add(tasks)
    db.session.commit()

    return {
        "name": tasks.name,
        "description": tasks.description,
        "duration": tasks.duration,
        "eisenhower_classification": eisenhower.type
    }

def importance_urgency_error(data):
        
    return {
                "error": {
                    "valid options": {
                        "importance": [
                            1,
                            2
                        ],
                        "urgency": [
                            1,
                            2
                        ]
                    },
                    "received options": {
                        "importance": data["importance"],
                        "urgency": data["urgency"]
                    }
                }
            }
            
