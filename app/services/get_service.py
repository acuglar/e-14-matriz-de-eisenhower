from app.models.category_model import CategoryModel

def get_all():

    categories = CategoryModel.query.all()
    
    return  [
                {
                    "category": {
                        "name": category.name,
                        "description": category.description,
                        "task": [
                            {
                                "name": task.name,
                                "description": task.description,
                                "priority": task.eisenhower.type
                            }
                            for task in category.tasks
                        ]
                    }
                    for category in categories
                }              
            ]