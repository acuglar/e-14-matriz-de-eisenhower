from app import db
from app.models.category_model import CategoryModel


def create_category(data: dict) -> dict:
    categories = CategoryModel(**data)

    db.session.add(categories)
    db.session.commit()

    return {
        "id": categories.id,
        "name": categories.name,
        "description": categories.description
    }
