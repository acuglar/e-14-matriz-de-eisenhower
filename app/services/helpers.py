def eisenhower_classification(task_urgency: int, task_importance: int) -> str:

    if task_urgency == 1 and task_importance == 1:
        eisenhower_type='Do It First'

    if task_urgency == 2 and task_importance == 1:
        eisenhower_type='Delegate It'

    if task_urgency == 1 and task_importance == 2:
        eisenhower_type='Schedule It'

    if task_urgency == 2 and task_importance == 2:
        eisenhower_type='Delete It'


    return eisenhower_type