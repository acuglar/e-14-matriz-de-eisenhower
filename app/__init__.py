from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from environs import Env
from app import views

env = Env()
env.read_env()

db = SQLAlchemy()

mg = Migrate()

def create_app():

    app = Flask(__name__)

    app.config["SQLALCHEMY_DATABASE_URI"] = env("SQLALCHEMY_DATABASE_URI")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["JSON_SORT_KEYS"] = False

    db.init_app(app)
    from app.models.category_model import CategoryModel
    from app.models.task_model import TaskModel
    from app.models.eisenhower_model import EisenhowerModel
    from app.models.task_category_model import TaskCategoryModel
    
    mg.init_app(app, db)

    views.init_app(app)

    return app
